from barcode import EAN13
from barcode import generate
from barcode.writer import ImageWriter
from random import randint
import qrcode


async def barcode(type_card, data_card):
	random_value = randint(1, 9999)

	if type_card == 'EAN13':
		data_card = data_card.replace("b", "")
		data_card = data_card.replace("'", "")
		card = EAN13(data_card, writer=ImageWriter())
		card.save('code'+str(random_value))
		return random_value

	elif type_card == 'CODE128':
		
		data_card = data_card.replace("b", "")
		data_card = data_card.replace("'", "")
		generate('CODE128', data_card, writer=ImageWriter(), output='code'+str(random_value))
		return random_value

	elif type_card == 'QRCODE':

		
		data_card = data_card.replace("b", "")
		data_card = data_card.replace("'", "")
		img = qrcode.make(data_card)
		img.save('code'+str(random_value)+'.png')

		'''
		#Второй способ, больше функционала.
		qr = qrcode.QRCode(version = 1, error_correction = qrcode.constants.ERROR_CORRECT_L, box_size = 10, border = 7)
		
		#ERROR_CORRECT_L: можно исправить около 7% или меньше ошибок.
		#ERROR_CORRECT_M: можно исправить около 15% или меньше ошибок.
		#ERROR_CORRECT_Q: можно исправить около 25% или меньше ошибок.
		#ERROR_CORRECT_H: можно исправить около 30% или меньше ошибок.
		
		qr.add_data(data_card)
		qr.make(fit = True)
		img = qr.make_image(fill_color = "black", back_color = "white")
		img.save('code'+str(random_value)+'.png')
		'''
	
		return random_value

	else:
		pass