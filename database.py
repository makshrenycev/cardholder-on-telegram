import sqlite3

async def registration_card(id_user, name_card, data_card, type_card):
	conn = sqlite3.connect('cards.db')
	cur = conn.cursor()
	cur.execute("INSERT INTO users_cards VALUES (?, ?, ?, ?)", (id_user, name_card, data_card, type_card, ))
	conn.commit()

async def check_cards(id_user):
	conn = sqlite3.connect('cards.db')
	cur = conn.cursor()
	cards = cur.execute("SELECT cards FROM users_cards WHERE id = ?", (id_user, )).fetchall()
	user_cards = []
	for card in cards:
		user_cards.append(card[0])
	return user_cards

async def check_data_type_card(id_user, name_card):
	conn = sqlite3.connect('cards.db')
	cur = conn.cursor()
	data_card = cur.execute("SELECT data, type FROM users_cards WHERE id = ? AND cards = ?", (id_user, name_card, )).fetchone()
	return data_card

async def check_card_name(id_user, name_card):
	conn = sqlite3.connect('cards.db')
	cur = conn.cursor()
	ids = cur.execute("SELECT id FROM users_cards WHERE cards = ?", (name_card, )).fetchall()
	for users in ids:
		if id_user == users[0]:
			return 'ok'
			break
		else:
			pass

async def delete_card(id_user, name_card):
	conn = sqlite3.connect('cards.db')
	cur = conn.cursor()
	cards = cur.execute("SELECT cards FROM users_cards WHERE id = ?", (id_user, )).fetchall()
	for card in cards:
		if name_card == card[0]:
			cur.execute("DELETE FROM users_cards WHERE id = ? AND cards = ?", (id_user, name_card, ))
			conn.commit()
			break
		else:
			pass

