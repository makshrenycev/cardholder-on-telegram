from PIL import Image
from pyzbar.pyzbar import decode
from random import randint
import os


async def decode_photo (msg, bot):
	image = await bot.get_file(msg.photo[-1].file_id)
	random_value = randint(1, 9999)
	path = 'qrcode'+str(random_value)+'.png'
	await image.download(path)
	try:
		result = decode (Image.open(path))
	except:
		pass
	if result == []:
		os.remove(path)
		return None
	else:
		os.remove(path)
		return result
	
