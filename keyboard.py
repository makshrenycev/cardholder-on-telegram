from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton

menu = ReplyKeyboardMarkup(resize_keyboard = True)
menu.row(KeyboardButton('💳 Мои карты')).row(KeyboardButton('➕ Добавить карту'))

cancel = ReplyKeyboardMarkup(resize_keyboard = True)
cancel.row(KeyboardButton('Отмена'))

async def delete_and_rename_user_card(id_user, name_card):
	delete_card = InlineKeyboardMarkup()
	delete_card.add(InlineKeyboardButton(text = 'Удалить', callback_data = 'del'+'/'+str(id_user)+'/'+str(name_card)))
	return delete_card



'''

start = ReplyKeyboardMarkup(resize_keyboard = True)

start_profile = KeyboardButton('📇 Профиль')
start_minigames = KeyboardButton('🎰 Мини-игры')
start_business = KeyboardButton('🏦 Бизнес')
start_help = KeyboardButton('💡 Помощь')
start_settings = KeyboardButton('⚙️')
start_info = KeyboardButton('📙 Инфо')

start.row(start_profile).row(start_minigames, start_business).row(start_help, start_settings, start_info)




buy_business_1 = InlineKeyboardMarkup()
buy_business_1.add(InlineKeyboardButton(text = 'Купить', callback_data = 'b1_buy'))
'''