import sqlite3
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton



async def show_my_cards(id):
	my_cards = ReplyKeyboardMarkup(resize_keyboard = True)
	conn = sqlite3.connect('cards.db')
	cur = conn.cursor()
	cards = cur.execute("SELECT cards FROM users_cards WHERE id = ?", (id, )).fetchall()
	for card in cards:
		my_cards.add(KeyboardButton(card[0]))
	my_cards.add(KeyboardButton('Назад'))
	return my_cards