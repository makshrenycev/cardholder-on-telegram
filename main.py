from aiogram import Bot, types
from aiogram.utils import executor
from aiogram.dispatcher import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import StatesGroup, State
import os

from config import TOKEN

from decode import decode_photo
import keyboard
import database
import my_cards
import generator

storage = MemoryStorage()
bot = Bot(token = TOKEN)
dp = Dispatcher(bot, storage = storage)



print ('-----Монитор-----')
print ('\nБот запущен.')



class FSM(StatesGroup):
	data_card = State()
	name_card = State()
	

@dp.message_handler(commands=['start'])
async def send_welcome(msg: types.Message):
	await bot.send_message(msg.from_user.id, '👋 Привет! \nЭто бот-кошелёк твоих скидочных карт. \n\nЕсли возникла проблема, писать сюда: @maax143', reply_markup = keyboard.menu)



@dp.message_handler(content_types=['text'])
async def menu(msg: types.Message):
	if msg.text == '💳 Мои карты':
		my_cards_hold = await my_cards.show_my_cards(msg.from_user.id)
		await bot.send_message(msg.from_user.id, '💳 Ваши скидочные карты: \n\n', reply_markup = my_cards_hold)

	elif msg.text == '➕ Добавить карту':
		await bot.send_message(msg.from_user.id, 'Пришлите фото штрих или QR кода карты.', reply_markup = keyboard.cancel)
		await FSM.data_card.set()

	elif msg.text == 'Назад':
		await bot.send_message(msg.from_user.id, 'Меню: ', reply_markup = keyboard.menu)

	elif msg.text in await database.check_cards(msg.from_user.id):
		data_card = await database.check_data_type_card(msg.from_user.id, msg.text)
		img = await generator.barcode(data_card[1], data_card[0])
		data_card = data_card[0].replace("b", "")
		data_card = data_card.replace("'", "")
		try:
			await bot.send_photo(msg.from_user.id, photo = types.InputFile('code'+str(img)+'.png'), caption = f'Название карты: {msg.text}\nДанные: {data_card}', reply_markup = await keyboard.delete_and_rename_user_card(msg.from_user.id, msg.text))
		except:
			await bot.send_message(msg.from_user.id, 'Не удалось отправить фото карты, пожалуйста, попробуйте ещё раз.')
		os.remove('code'+str(img)+'.png')





@dp.message_handler(content_types=['photo', 'text'], state = FSM.data_card)
async def data_card(msg: types.Message, state: FSMContext):


	if msg.text == 'Отмена':
		await state.finish()
		await bot.send_message(msg.from_user.id, 'Отменено.', reply_markup = keyboard.menu)
	else:
		result = await decode_photo(msg, bot)

		if result == None:
			await bot.send_message(msg.from_user.id, 'Штрих или QR код на фото не обнаружен. \nВозможно, не удалость его распознать, отправьте фото ещё раз.', reply_markup = keyboard.cancel)
			await FSM.data_card.set()
		else:
			await state.update_data(data_card = result[0].data)
			await state.update_data(type_card = result[0].type)
			await bot.send_message(msg.from_user.id, 'Введите название карты: ', reply_markup = keyboard.cancel)
			await FSM.next()



@dp.message_handler(content_types=['text'], state = FSM.name_card)
async def write_name_card(msg: types.Message, state: FSMContext):
	if msg.text == 'Отмена':
		await state.finish()
		await bot.send_message(msg.from_user.id, 'Отменено.', reply_markup = keyboard.menu)
	else:
		if len(msg.text) <= 20:
			check_card = await database.check_card_name(msg.from_user.id, msg.text)
			if check_card == None:
				try:
					user_data = await state.get_data()
					data_card = str(user_data['data_card'])
					type_card = str(user_data['type_card'])
					name_card = str(msg.text)
					await database.registration_card(msg.from_user.id, name_card, data_card, type_card)
					await bot.send_message(msg.from_user.id, 'Карта успешно добавлена!', reply_markup = keyboard.menu)

					await state.finish()
				except:
					await bot.send_message(msg.from_user.id, 'Произошла ошибка! Попробуйте ещё раз.', reply_markup = keyboard.cancel)
					await FSM.name_card.set()
			else:
				await bot.send_message(msg.from_user.id, 'У вас уже есть карта с таким названием, пожалуйста, введите другое название.')
				await FSM.name_card.set()

		else:
			await bot.send_message(msg.from_user.id, 'Длина названия не может привышать 20-ти символов.', reply_markup = keyboard.cancel)
			await FSM.name_card.set()

@dp.callback_query_handler(lambda call: call.data.startswith('del'))
async def del_card(call: types.CallbackQuery):
	text = call.data.split('/')
	try:
		await database.delete_card(text[1], text[2])
		await bot.delete_message(chat_id=call.from_user.id, message_id=call.message.message_id)
		my_cards_hold = await my_cards.show_my_cards(call.from_user.id)
		await call.answer(f'Карта {text[2]} успешно удалена!', show_alert=False)
		await bot.send_message(call.from_user.id, 'Список карт обновлён.', reply_markup = my_cards_hold)
		

	except:
		await bot.send_message(call.from_user.id, 'Не удалось удалить карту, пожалуйста, попробуйте ещё раз!', reply_markup = keyboard.menu)



if __name__ == '__main__':
	executor.start_polling(dp, skip_updates = True)